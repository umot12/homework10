package homework8.Test;

import homework8.Family.Family;
import homework8.Family.Fish;
import homework8.Family.Human;
import homework8.Family.Pet;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTest {
    private Human module;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    @Test
    public void  humanToString(){

        module = new Human("human","Golovach",1995);
        String actual = module.toString();
        String expected = "Human{name = human surname = Golovach year = 1995 iq = 0 schedule = null}";
        assertEquals(expected,actual);
    }
    @Test

    public void testHumanGreetPet(){
        PrintStream old=System.out;
        Human human=new Human();
        Human mother = new Human();
        Human father = new Human();
        Family Smith = new Family(mother,father);
        Pet Mira = new Fish("Myki",5,(byte) 60);
        Set pet = new HashSet<>(Set.of(Mira));
        Smith.setPet(pet);
        System.setOut(new PrintStream(output));
        human.greetPet(Mira);
        assertEquals(output.toString().replaceAll("\n",""),"Привет Myki","Successfully brings text");

        System.setOut(old);
    }
    @Test

    public void testHumanDescribePet(){
        PrintStream old=System.out;
        Human human=new Human();
        Human mother = new Human();
        Human father = new Human();
        Family Smith = new Family(mother,father);
        Pet Mura = new Fish("Myli",5,(byte) 60);
        Set pet = new HashSet<>(Set.of(Mura));
        Smith.setPet(pet);
        System.setOut(new PrintStream(output));
        human.describePet(Mura);
        assertEquals(output.toString().replaceAll("\n",""),"У меня есть FISH, ему 5 лет, он очень хитрый","Successfully brings text");

        System.setOut(old);
    }
}
