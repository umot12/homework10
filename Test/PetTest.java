package homework8.Test;

import homework8.Family.Dog;
import homework8.Family.DomesticCat;
import homework8.Family.Pet;
import homework8.Family.RoboCat;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTest {
    private Pet module;

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Test
    public void  petToString(){
        module = new DomesticCat("Lina",5,(byte) 60);
        String actual = module.toString();
        String expected = "DOMESTICCAT{nickname=Lina, age=5, trickLevel=60 , habits=null}";
        assertEquals(expected,actual);
    }
    @Test

    public void testPetEat(){
        PrintStream old=System.out;
        Pet pet=new Dog("Java",3,(byte) 50);
        System.setOut(new PrintStream(output));
        pet.eat();
        assertEquals(output.toString().replaceAll("\n",""),"Я кушаю","Successfully brings text");

        System.setOut(old);
    }

    @Test

    public void testDogFoul(){
        PrintStream old=System.out;
        Dog pet=new Dog("java",3,(byte) 70);
        System.setOut(new PrintStream(output));
        pet.foul();
        assertEquals(output.toString().replaceAll("\n",""),"Нужно замести следы ...","Successfully brings text");

        System.setOut(old);
    }
    @Test

    public void testCatFoul(){
        PrintStream old=System.out;
        DomesticCat pet=new DomesticCat("Lina",3,(byte) 70);
        System.setOut(new PrintStream(output));
        pet.foul();
        assertEquals(output.toString().replaceAll("\n",""),"Поточу ка я когти об этот диван ...","Successfully brings text");

        System.setOut(old);
    }
    @Test

    public void testRoboCatFoul(){
        PrintStream old=System.out;
        RoboCat pet=new RoboCat("K5",3,(byte) 70);
        System.setOut(new PrintStream(output));
        pet.foul();
        assertEquals(output.toString().replaceAll("\n",""),"Роботы не делают глупостей ...","Successfully brings text");

        System.setOut(old);
    }
    @Test
    public void testCatRespond(){
        PrintStream old=System.out;
        DomesticCat pet=new DomesticCat("Lina",5,(byte) 60);
        System.setOut(new PrintStream(output));
        pet.respond();
        assertEquals(output.toString().replaceAll("\n",""),"Привет хозяин Я DOMESTICCAT Lina cоскучилcя","Successfully brings text");

        System.setOut(old);
    }
    @Test
    public void testDogRespond(){
        PrintStream old=System.out;
        Dog pet=new Dog("Java",5,(byte) 60);
        System.setOut(new PrintStream(output));
        pet.respond();
        assertEquals(output.toString().replaceAll("\n",""),"Привет хозяин Я DOG Java cоскучилcя","Successfully brings text");

        System.setOut(old);
    }
}


