package homework8;

import homework8.DAO.CollectionFamilyDao;
import homework8.DAO.FamilyController;
import homework8.DAO.FamilyDao;
import homework8.DAO.FamilyService;
import homework8.Family.Dog;
import homework8.Family.Family;
import homework8.Family.Human;
import homework8.Family.RoboCat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        long millisInDay = 24*3600*1000;
        LocalDate dateLoc1 =  LocalDate.of(1999,11,13);
        long date1 = dateLoc1.toEpochDay()*millisInDay;
        LocalDate dateLoc2 =  LocalDate.of(1909,6,10);
        long date2 = dateLoc2.toEpochDay()*millisInDay;
        LocalDate dateLoc3 =  LocalDate.of(2004,2,4);
        long date3 = dateLoc3.toEpochDay()*millisInDay;
        LocalDate childye1 = LocalDate.of(2021,10,26);
        long chilarg12 = childye1.toEpochDay()*millisInDay;//возраст младенца
        LocalDate childye = LocalDate.of(2015,10,26);
        long chilarg = childye.toEpochDay()*millisInDay;//возраст биг
        List<Family> familyList = new ArrayList<>(List.of(
                new Family(new Human("pap1","gag1",date1),new Human("mam1","gag1",date3)),
                new Family(new Human("pap2","gag2",date1),new Human("mam2","gag2",date1)),
                new Family(new Human("pap3","gag3",date1),new Human("mam3","gag3",date1)),
                new Family(new Human("pap4","gag4",date2),new Human("mam4","gag4",date3))
        ));
        FamilyDao familyDao = new CollectionFamilyDao(familyList);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.getAllFamilies().forEach(el -> el.toString());
        System.out.println("----------------------------------");
        familyController.displayAllFamilies();
        System.out.println("----------------------------------");
        System.out.println(familyController.count());
        System.out.println("----------------------------------");
        familyController.getFamilyById(3).toString();//pap4 and mam4
        System.out.println("----------------------------------");
        familyController.adoptChild(familyList.get(2),new Human("Oleg","gag3",chilarg));
        familyController.getFamilyById(2).toString();
        System.out.println("----------------------------------");
        familyController.bornChild(familyList.get(0),"Igor","Inna");
        familyController.getFamilyById(0).toString();
        System.out.println("----------------------------------");
        familyController.getFamiliesBiggerThen(3).forEach(el -> el.toString());
        System.out.println("----------------------------------");
        familyController.getFamiliesBiggerThen(2).forEach(el -> el.toString());
        System.out.println("----------------------------------");
        System.out.println("Number of families:" + familyList.size());
        System.out.println("|||||||||||||||||");
        System.out.println("Number of families with 3 members: " + familyController.countFamiliesWithMemberNumber(3));
        System.out.println("----------------------------------");
        System.out.println("Last family in list before applying method :");
        familyList.get(familyList.size() -1).toString();
        System.out.println("Last family in list after applying method :");
        familyController.createNewFamily(new Human("pap5","gag5",date2),new Human("mam5","gag5",date2));
        familyList.get(familyList.size() -1).toString();
        System.out.println("----------------------------------");
        familyController.addPet(1,new Dog("guk2",3,(byte) 100));
        familyController.addPet(1,new Dog("guk48",10,(byte) 300));
        familyController.getFamilyById(1).toString();
        System.out.println("----------------------------------");
        Arrays.toString( familyController.getPets(1).toArray());
        //нету животных ->  null
        if(familyController.getPets(0).size() == 0){
            System.out.println("null");//+
        }else
            Arrays.toString( familyController.getPets(0).toArray());// count -> 0
        System.out.println("----------------------------------");
        familyList.get(2).toString();//  было два родителя с индексом 3 + ребенок
        familyController.deleteFamilyByIndex(2);//удалили их
        familyList.get(2).toString();// все здинулось на 1, теперь два родителя со след интексом в этой ячейки
        System.out.println("----------------------------------");
        for (int i = 0; i < familyList.size(); i++) {
            familyList.get(i).getChildren().add(new Human("child1", "BIGGGG", chilarg));
            familyList.get(i).getChildren().add(new Human("FFFFFF", "MLODENETS",chilarg12));
        }
        int age = 5;
        familyController.displayAllFamilies();//если биг и млоденец
        familyController.deleteAllChildrenOlderThen(age);//удаляет биг ибо ему больше 5 лет
        System.out.println("delet 5+ age");
        familyController.displayAllFamilies();// остаеться млоденец


    }
}
